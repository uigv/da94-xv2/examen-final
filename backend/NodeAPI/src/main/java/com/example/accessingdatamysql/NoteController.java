package com.example.accessingdatamysql;

import com.example.accessingdatamysql.model.ResponseEntity;
import org.aspectj.weaver.ast.Not;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping(path="/api")
public class NoteController {

    @Autowired
    private NoteRepository noteRepository;

    @PostMapping(path="/notes")
    public @ResponseBody
    ResponseEntity add (@Valid @RequestBody Note nNote) {
        try{
            Note note = new Note();
            note.setTitle(nNote.getTitle());
            note.setDescription(nNote.getDescription());
            note.setFavorite(nNote.isFavorite());
            note.setColor(nNote.getColor());
            note.setDate(nNote.getDate());

            noteRepository.save(note);
        }catch (Exception e){
            return new ResponseEntity(100,"Error : No se agregó este elemento",null);
        }

        return new ResponseEntity(200, "Saved",null);
    }

    @PutMapping("/notes/{id}")
    public @ResponseBody ResponseEntity update(
            @PathVariable(value = "id") Integer noteId,
            @Valid @RequestBody Note noteUpdate){
        try{
            Optional<Note> noteOptional = noteRepository.findById(noteId);
            if(noteOptional.isPresent()){
                Note note= noteOptional.get();
                note.setTitle(noteUpdate.getTitle());
                note.setDescription(noteUpdate.getDescription());
                note.setFavorite(noteUpdate.isFavorite());
                note.setColor(noteUpdate.getColor());
                note.setDate(noteUpdate.getDate());

                noteRepository.save(note);
            }else{
                return new ResponseEntity(100,"Error : Este elemento no existe",null);
            }
        }catch (Exception e){
            return new ResponseEntity(100,"Error : "+e.getMessage(),null);
        }
        return new ResponseEntity(200, "Updated",null);
    }

    @DeleteMapping("/notes/{id}")
    public @ResponseBody ResponseEntity remove(@PathVariable(value = "id") String noteId){
        try{
            Integer id = Integer.parseInt(noteId);
            noteRepository.deleteById(id);
        }catch (Exception e){
            return new ResponseEntity(100,"Error : "+e.getMessage(),null);
        }
        return new ResponseEntity(200, "Deleted",null);
    }

    @GetMapping(path="/notes")
    public @ResponseBody
    List<Note> getAll() {
        return noteRepository.findAll();
    }
}
