package com.example.accessingdatamysql;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface NoteRepository  extends CrudRepository<Note, Integer> {
    @Override
    List<Note> findAll();
}
