package com.example.accessingdatamysql;

import javax.persistence.*;

@Entity
@Table(name = "categoria")
public class Category {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY) //SEQUENCE @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="idCategoria",updatable = false, nullable = false)
    private Integer id;

    @Column(name="Categoria")
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
