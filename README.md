# Examen Final

Examen Final - Trabajo académico

Desarrollar una solución Móvil Web Services Consumidor, con las siguientes características:


1. Debe implementar una aplicación Movil en Android.

2. Debe Crear el proyecto Móvil en Android Studio

3. Debe crear un proyecto proveedor en tecnología Java con una Base de Datos

4. Debe crear el consumidor movil, una solución tipo Json o Rest o Soap para que pueda consumir servicios del aplicativo

5. Realizar pruebas de consultas del consulta del consumidor y respuesta del proveedor

6. Debe publicar y explicar, con voz y vídeo, explicando la solución.

7. Adjuntar el archivo con el URL (archivo de texto) el vídeo publicado en YouTube.

Nota: Se reitera publicar el Video de la solución.

No se aceptan trabajos que se hayan desarrollan en estas sesiones

## Entregable 

Se realizó una app para crear notas, donde te permite agregar, actualizar, remover y listar notas.

Para este proyecto se utilizó lo siguiente :

- Backend : La base de datos se realizó con mysql y la BD fue llamada "noteapp", para los servicios se utilizó spring-boot y jpa.

- App : La app conecta con el API del backend y se realizó con Kotlin.

# Backend

El proyecto esta hecho con Java, el framework Spring boot, jpa y Gradle. El IDE utilizado fue Intellij IDEA https://www.jetbrains.com/idea/

## URL

http://localhost:9090/


## API (endpoints)

```
http://localhost:9090/api/
```

- Listado de notas (GET)

```
http://localhost:9090/api/notes
```

```
[
    {
        "id": 1,
        "title": "Reunión huawei",
        "description": "Notas de reunión",
        "favorite": true,
        "color": "#ff00ff",
        "date": "Fri Nov 15 17:16:27 UTC 2019"
    },
    {
        "id": 3,
        "title": "Demo",
        "description": "Nota demo",
        "favorite": false,
        "color": "#ffff00",
        "date": "Fri Nov 15 17:17:41 UTC 2019"
    }
]
```

- Agregar una nota (POST)

```
http://localhost:9090/api/notes
```

Se envía un body raw en Json

```
 {
        "title": "Demo 3",
        "description": "Nota demo 3",
        "favorite": false,
        "color": "#ffff00",
        "date": "Fri Nov 15 17:17:41 UTC 2019"
 }
```

- Editar una nota (PUT)

```
http://localhost:9090/api/notes/4
```

Se envía un body raw en Json

```
 {
        "title": "Demo 4 editado",
        "description": "Nota demo 4 editado ",
        "favorite": true,
        "color": "#ffff00",
        "date": "Fri Nov 15 17:17:47 UTC 2019"
 }
```

- Eliminar una nota (DELETE)

```
http://localhost:9090/api/notes/4
```

## Ejecutar el proyecto 


Limpiar proyecto

```
./gradlew clean
```

Ejecutar proyecto

```
./gradlew build && java -jar build/libs/NodeAPI-0.0.1-SNAPSHOT.jar
```

# App

- El proyecto esta realizado con Kotlin, es una aplicación nativa y se utilizó Android Studio.

# Referencias

- https://spring.io/guides/gs/spring-boot/
- https://spring.io/guides/gs/accessing-data-mysql/
