package com.emedinaa.kotlinapp.storage

import com.emedinaa.kotlinapp.model.NoteEntity

open class BaseResponse(val status:Int?,val msg:String?){
    companion object {
        const val STATUS_CODE:Int=200
    }

    protected fun isSuccess():Boolean {
        return status == STATUS_CODE
    }
}

class NoteResponse(status:Int?, val data: NoteEntity?, msg:String?):BaseResponse(status,msg)

class NotesResponse(status:Int?,val data:List<NoteEntity>?, msg:String?):BaseResponse(status,msg)










