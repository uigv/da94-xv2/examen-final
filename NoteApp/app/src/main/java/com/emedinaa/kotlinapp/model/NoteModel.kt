package com.emedinaa.kotlinapp.model

import java.io.Serializable

data class NoteEntity(val id:Int?, val title:String?, val description:String?,
                      val favorite:Boolean?,val color:String?,val date:String?):Serializable