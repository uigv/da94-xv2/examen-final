package com.emedinaa.kotlinapp.storage

data class NoteRaw(val title:String?, val description:String?,
                   val favorite:Boolean?, val color:String,val date:String)