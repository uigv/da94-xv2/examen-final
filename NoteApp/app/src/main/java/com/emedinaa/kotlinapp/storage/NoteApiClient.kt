package com.kotlin.samples.kotlinapp.storage

import com.emedinaa.kotlinapp.model.NoteEntity
import com.emedinaa.kotlinapp.storage.*
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import retrofit2.http.DELETE
import java.util.concurrent.TimeUnit

object NoteApiClient {

    //private val API_BASE_URL = "http://10.0.2.2:9090/"
    private val API_BASE_URL = "http://10.0.3.2:9090/"
    private var servicesApiInterface:ServicesApiInterface?=null

    fun build():ServicesApiInterface?{
        var builder: Retrofit.Builder = Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())

        var httpClient: OkHttpClient.Builder = OkHttpClient.Builder()
        httpClient.connectTimeout(60, TimeUnit.SECONDS)
        httpClient.addInterceptor(interceptor())

        var retrofit: Retrofit = builder.client(httpClient.build()).build()
        servicesApiInterface = retrofit.create(
                ServicesApiInterface::class.java)

        return servicesApiInterface as ServicesApiInterface
    }

    private fun interceptor(): HttpLoggingInterceptor {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level=HttpLoggingInterceptor.Level.BODY
        return httpLoggingInterceptor
    }

    interface ServicesApiInterface{

        //notes
        @GET("api/notes")
        fun notes(): Call<List<NoteEntity>>

        @POST("api/notes")
        fun addNote(@Body raw: NoteRaw): Call<Any>

        //update
        @PUT("api/notes/{id}")
        fun updateNote(@Path("id") noteId: Int?, @Body raw: NoteRaw): Call<Any>

        //delete
        @DELETE("api/notes/{id}")
        fun deleteNote(@Path("id") noteId: Int?): Call<Any>

    }
}